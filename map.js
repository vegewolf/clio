module.exports = function(){

  Object.prototype.map = function(func){
    //get an array of keys
    var keys_arr = Object.keys(this);

    //get a reference to the object
    var that = this;

    //iterate through all the properties of the object/hash
    //and apply the mapping function
    keys_arr.forEach(function(key){

      //get the value
      var value = that[key];

      //run the mapping function with key and value arguments
      var map_item_arr = func(key, value);

      //make sure that the mapping function returns an array that has 2 items in it
      if (!Array.isArray(map_item_arr) || map_item_arr.length !== 2){
        throw new Error('"Object.prototype.map()": the mapping function should return an array that has length of 2');
      };

      //create variables for the new key and the new value
      var new_key = map_item_arr[0];
      var new_value = map_item_arr[1];

      //create a new key with a new value
      that[new_key] = new_value;

      //delete the old property
      delete that[key];
    });
  };
};
