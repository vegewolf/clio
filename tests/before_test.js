var expect = require('chai').expect;
var _before = require('../before')

describe('Write a simple way of adding a before filter onto a given method of a class', function() {

  var BClass = function(output){
    this.output = output || '';
  };

  BClass.prototype.talk = function(words){
    this.output += words;
  };


  it('shoud prepend a given function, to the given method of an object, with its context', function(){

    var b = new BClass('Clio is Hiring');
    _before(b, 'talk', function(words) { this.output += " Alan, because he is"});
    b.talk(" a java script ninja.");

    expect(b.output).to.be.equal("Clio is Hiring Alan, because he is a java script ninja.");
    //console.log(b.output);
  });

  it('shoud pass in the argument from the method to the prepended function ', function(){

    var b = new BClass('Clio is Hiring ');
    _before(b, 'talk', function(words) { this.output += words.toUpperCase() + ', ';});
    b.talk("Alan");

    expect(b.output).to.be.equal("Clio is Hiring ALAN, Alan");
    //console.log(b.output);
  });

  it('should work with conole.logs as prescribed', function(){
    var AClass = function(){};

    AClass.prototype.talk = function(words){
      console.log(words);
    };

    //create an object 'a', instance of Aclass
    a = new AClass;

    _before(a, 'talk', function() { console.log("I'm First!")  });
    a.talk("Hello");
  });
});



// Logs:
//   “Hello”

//a.before('talk', function() { console.log(“I'm First!”)  });

// OR

//before(a, 'talk', function() { console.log(“I'm First!”)  });

//a.talk(“Hello”);
// Logs:
//   “I'm First!”
//   “Hello”
