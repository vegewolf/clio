var expect = require('chai').expect;
var findIt = require('../grid');


describe('We have a grid of mostly 0s there will be one 1. Write a function that returns the position of 1.',
function() {
  var grid1 = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 1]
  ];
  // => [2, 0]

  var grid2 = [
    [0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
  ];
  // => [4,5]

  var grid3 = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
  ];
  // => [2,3]
  it('should return the position of the 1 relative to the bottom left of the grid', function(){
    expect(findIt(grid1)).to.eql([2, 0]);
    expect(findIt(grid2)).to.eql([4, 5]);
    expect(findIt(grid3)).to.eql([2, 3]);

  });
});
