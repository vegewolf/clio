var expect = require('chai').expect;
require('../map')();

describe('Options to perform a map operation on a hash', function() {

  it('should take a hash and perform a map operation on the keys and values.', function(){
    var myHash = {
     'a': 1,
     'b': 2,
     'c': 3
    };

    var myHash2 = {
      'a': 'alan',
      'b': 'chris',
      'c': 'deamon'
    };

    var myHashMapped = {
      A: 2,
      B: 3,
      C: 4
    };

    var myHash2Mapped = {
      a1: 'ALAN',
      b2: 'CHRIS',
      c3: 'DEAMON'
    };

    myHash.map(function(key, value) {
     return [key.toUpperCase(), value + 1]
    });

    var counter = 0;
    myHash2.map(function(key, value) {
      counter++;
     return [key + counter, value.toUpperCase()]
    });

    expect(myHash).to.eql(myHashMapped);
    expect(myHash2).to.eql(myHash2Mapped);

  });
});
