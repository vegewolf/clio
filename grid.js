//We have a grid of mostly 0s there will be one 1. Write a function that returns the position of the 1 relative to the bottom left of the grid.

function findIt(grid){

  //iterate through the grid array
  for( var i = 0; i <= grid.length - 1; i++){
    //asign given row to a row variable
    var row = grid[i];

    //iterate throgh the items of the row
    for( var j = 0; j <= row.length - 1; j++){
      if (row[j] === 1) {

        //return an array
        //first item is a horisontal (x axis) position
        //second item is a vertical (y axis) position
        //from bottom left
        return [j, grid.length - 1 - i]
      };
    };
  };
};

module.exports = findIt;
