function before(obj, methodName, funcToAdd){

  //Check if the arguments have proper types
  if (typeof obj !== 'object'){ throw new Error('"before()": the first argument should be an object')};
  if (typeof methodName !== 'string'){ throw new Error('"before()": the second argument should be a string')};
  if (typeof funcToAdd !== 'function'){ throw new Error('"before()": the third argument should be a function')};

  //Check if a given method exists
  if (!obj[methodName]){ throw new Error('"before()": given object has no "' + methodName + '" method.')};

  //given method of the object
  var method = obj[methodName];

  //prepend function to the given method
  obj[methodName] = function(words){
    //prepended function - I'm using 'call' to pass the context of the object, should the funcToAdd require 'this'.
    //add 'words' parameter,should the
    funcToAdd.call(obj, words);
    method.call(obj, words); //given method
  };
};

module.exports = before;

/*
var AClass = function(){};

AClass.prototype.talk = function(words){
  console.log(words);
};

//create an object 'a', instance of Aclass
a = new AClass;

before(a, 'talk', function() { console.log("I'm First!")  });
a.talk("Hello");
*/
